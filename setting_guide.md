macOS 기준으로 설명함



### 학습할 이미지 폴더 세팅

1. 학습할 이미지가 들어있는 폴더 준비 => 아래에서는 이 폴더를 '홈폴더' 라고 부르겠음
2. 홈폴더 안에 이미지가 들어있는 폴더 넣기
3. 홈폴더의 명칭은 어떻게 해도 관계없음. 이미지 파일은 다음과 같은 규칙으로 저장되어 있어야 함
   - 홈폴더 / 나이 / 개인번호 / 이미지 파일
   - 각 이미지 파일은 4가지 각도에서 찍은 xray 사진이며 같은 각도의 사진은 파일명의 끝번호가 같게 끝나야 함(ex. 0001.jpg, 0002.jpg)
   - 이미지 확장자는 jpg 를 사용함. 기타 jpeg 이나 png 등은 지원하지 않음
   - 가급적 파일명에 한글이 포함되지 않는 것을 추천(영문 및 숫자만 포함)
   - 아래의 캡쳐된 이미지는 CR 을 홈폴더로 하는 예시 폴더 및 파일 구조임
     <img src="./img/image-20191031153600276.png" alt="image-20191031153600276" style="zoom:50%;" />



### 도커 실행

1. 도커 서버 설정 => 메모리 부족 겪지 않게 램 높게 설정
   <img src="./img/image-20191031153757613.png" alt="image-20191031153757613" style="zoom:33%;" />

2. 도커 이미지 다운로드

   ```
   docker pull moondokcer/xray:1.1
   ```

   

3. 도커 컨테이너 실행 및 볼륨 마운트
   
   * 학습할 원본 이미지가 들어있는 홈폴더를 실행할 도커 컨테이너의 /xray/original_images 에 볼륨 마운트함
   * 주의 : 컨테이너 내부의 경로(/xray/original_images)는 변경되면 안됨
   * 아래 예시는 명칭이 CR 인 홈폴더를 볼륨 마운트하여 실행한 코드임
   
   ```
   docker run --rm -it -v /Users/name/playground/xray_v1/backend/CR:/xray/original_images moondokcer/xray:1.1 bash
   ```
   
   

### 이미지 재분류 및 학습

실행된 도커의 터미널 안에서 원본 이미지 preprocessing 및 학습 진행
먼저 파이썬 파일이 저장된 위치로 이동

```
cd /xray
```



1. 원본 이미지를 학습에 알맞게 resize 및 클래스 분류
   
   * 해당 폴더에는 image_resave.py, transfer_xray.py 의 파일 두개 및 볼륨 마운트한 original_images 폴더가 존재함
   * image_resave.py 를 파이썬 프로그램으로 실행하면 자동으로 original_images 에 저장된 이미지를 재분류 및 크기 조정하여 그 하위에 dataset 이라는 폴더를 생성하여 저장함

   ```
   python image_resave.py
   ```

   

2. 재분류된 이미지를 학습

   * 이미지가 성공적으로 재분류되면 original_images 폴더 아래에 dataset 이라는 폴더가 생성되고 그 아래에 연령 및 촬영각도별로 20개의 폴더로 분류된 이미지들이 저장되어 있을 것임
   * transfer_xray.py 를 실행하면 dataset 에 저장되어 있는 이미지를 학습하며 학습이 끝나면 xray_model.h5 모델을 저장하게 됨
   <img src="./img/image-20191031151751226.png" alt="image-20191031151751226" style="zoom:50%;" />


3. 학습 epoch 조정
   * 실행한 도커 컨테이너 내부의 transfer_xray.py 를 수정해야 함
   * 컨테이너 내에 nano 가 내장되어 있음. nano 로 transfer_xray.py 실행
   * 가장 하단으로 내리면 epochs=2 에서 원하는 숫자로 고쳐주고 control + X 를 눌러서 저장

   <img src="./img/image-20191031160323932.png" alt="image-20191031160323932" style="zoom:50%;" />

   
   early stopping 이 기본으로 설정되어 있으므로 epoch 을 높은 수로 설정하여도 됨

