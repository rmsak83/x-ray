from keras.applications import ResNet50
from keras.preprocessing.image import img_to_array
from keras.applications import imagenet_utils
from PIL import Image
import numpy as np
import flask
import io
from flask import Flask, render_template, request, redirect, url_for
from werkzeug import secure_filename

app = flask.Flask(__name__)
model = None


def load_model():
    global model
    model = ResNet50(weights="imagenet")


def prepare_image(image, target):
    if image.mode != "RGB":
        image = image.convert("RGB")

    image = image.resize(target)
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)
    image = imagenet_utils.preprocess_input(image)

    return image


def predict(file_path):
    data = {"success": False}
    image = Image.open(file_path)
    image = prepare_image(image, target=(224, 224))

    preds = model.predict(image)
    results = imagenet_utils.decode_predictions(preds)
    data["predictions"] = []

    for (imagenetID, label, prob) in results[0]:
        r = {"label": label, "probability": float(prob)}
        data["predictions"].append(r)

    data["success"] = True
    print(data)
    return data


@app.route("/")
def hello():
    return render_template("main.html")


'''
@app.route("/predict", methods=["POST"])
def predict():
    data = {"success": False}

    if flask.request.method == "POST":
        if flask.request.files.get("image"):
            image = flask.request.files["image"].read()
            image = Image.open(io.BytesIO(image))

            image = prepare_image(image, target=(224, 224))

            preds = model.predict(image)
            results = imagenet_utils.decode_predictions(preds)
            data["predictions"] = []
            print(data)

            for (imagenetID, label, prob) in results[0]:
                r = {"label": label, "probability": float(prob)}
                data["predictions"].append(r)

            data["success"] = True

    return flask.jsonify(data)'''


@app.route('/uploader', methods=['GET', 'POST'])
def upload_file_resnet():
    if request.method == 'POST':
        f = request.files['file']
        f.save(
            "/Users/geunminmoon/playground/kerasREST/" +
            secure_filename(
                f.filename))
        alert_text = 'file uploaded successfully'
        file_path = '/Users/geunminmoon/playground/kerasREST/' + f.filename
        print(file_path)

    data = predict(file_path)
    '''
    data = {"success": False}
    image = Image.open(file_path)
    image = prepare_image(image, target=(224, 224))

    preds = model.predict(image)
    results = imagenet_utils.decode_predictions(preds)
    data["predictions"] = []

    for (imagenetID, label, prob) in results[0]:
        r = {"label": label, "probability": float(prob)}
        data["predictions"].append(r)

    data["success"] = True
    '''

    return render_template(
        'show_result_resnet.html',
        alert_text=alert_text,
        file_path=file_path, file_data=data["predictions"])
    # return 'file uploaded successfully'


if __name__ == "__main__":
    print(("* Loading Keras model and Flask starting server..."
           "please wait until server has fully started"))
    load_model()
    app.run(threaded=False)
